## Adott egy mozi, annak egy terme, abban 2 szabad szék. 

1. Valósítsa meg, hogy weben keresztül ezekre a szabad székekre rá lehessen foglalni!
2. Egy szabad székre egyidőben csak 1 sikeres foglalás lehetséges.
3. Egy felhasználó foglalhat 1 vagy 2 szabad széket is.
4. A foglalás sikertelen „fizetés” esetén 2 perc múlva szabaduljon fel!
5. A felhasználók lássák a székekről azok státuszát: „szabad”; „foglalt”; „elkelt”
6. Sikeres foglalás után fizetésképpen kérjük be a vevő email címét, ahová küldjünk emailt a
sikeres foglalásról, és változzon a szék státusza „elkelt”-re!
7. A feladat megoldásakor nem a megjelenítés a lényeg, hanem az egyidejű foglalások kizárásának
biztosítása.

A database/migrations mappában vannak a migrációk, laravel projectben php artisan migrate command futtatásával lehet létrehozni az adatbázist minta adatokkal.

futtatás : php artisan serve
websocket szervert lokálisan futtattam hozzá , 6001 porton, amin keresztül mentek a tranzakciók
php artisan websockets:serve

ha nem futna:

1. composer install

2. .env fájl kitötése adatbázis adatokkal

3. BROADCAST_DRIVER átírása logról pusher - re

4. php artisan key:generate

node modules telepítés

npm install

npm run dev