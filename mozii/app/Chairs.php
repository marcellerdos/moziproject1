<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chairs extends Model
{
    protected $table = 'chairs';
    protected $primaryKey ='id';

    protected $fillable = [

        'status_id', 'user_id'

    ];

    public function status()
    {
        return $this->belongsTo('App\Status');
    }
}
