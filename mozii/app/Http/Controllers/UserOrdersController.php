<?php

namespace App\Http\Controllers;

use App\Chairs;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserOrdersController extends Controller
{
    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $orders = Chairs::with('status')->where('user_id', $user->id)->get();

        return view('index', compact('user', 'orders'));
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $chairs = DB::table('chairs')->get();
        
        $user = auth()->user();
        return view('create')->with('chairs',$chairs)->with('user', $user);
    }

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arrayOfchairId = $request->input('toppings');

        foreach($arrayOfchairId as $chairId){
        /*$request->validate([
            'chair_id' => 'required',
        ]);*/

        $chairs = Chairs::where('id',$chairId)->first();

        $order = Order::create([
            'user_id' => auth()->user()->id,
            'chair_id' => $chairId,
        ]);

        if($chairs->user_id != auth()->user()->id && $chairs->user_id != null && $chairs->status_id = 2 || $chairs->status_id = 3)
        {
            return redirect()->route('user.orders.show', $order)->with('denyMessage', 'Foglalás Sikertelen! Már foglalt a szék!');
        }else{
        $chairs->update(['status_id' => 2, 'user_id' => auth()->user()->id]);
        }

        
        /*Chairs::where('id', $chairId)
        ->update(['status_id' => 2, 'user_id' => auth()->user()->id]);*/
        }

        return redirect()->route('user.orders.show', $order)->with('succesMessage', 'Foglalás Sikeres!');

      
    }
    
    public function destroy($id)
    {
        $user = auth()->user();
        $orders = Chairs::with('status')->where('user_id', $user->id)->get();
        $chairs = DB::table('chairs')->get();

        $foglalas = Chairs::find($id);
        $foglalas->update([
            'status_id' => 1,
            'user_id' => null
        ]);

        return redirect()->route('user.orders.create')->with('succesMessage', 'Sikeresen törölted a foglalást!')->with('chairs', $chairs);
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return view('show', compact('order'));
    }

    public function payShow(Order $order)
    {
        return view('payShow', compact('order'));
    }

    public function pay(Order $order)
    {
        $user = auth()->user();
        $allChair = DB::table('chairs')->get();
        $chairs = Chairs::where('id',$order->chair_id)->first();
        $chairs->update([
            'status_id' => 3
        ]);
        return redirect()->route('user.orders.create')->with('chairs',$allChair)->with('user',$user)->with('payMessage','Sikeres fizetés!');
    }
}
