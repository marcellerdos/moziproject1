@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">A foglalás státusza</div>

                <div class="panel-body">
                    @if (session('succesMessage'))
                        <div class="alert alert-success">
                            {{ session('succesMessage') }}
                        </div>
                    @elseif(session('denyMessage'))
                    <div class="alert alert-danger">
                        {{ session('denyMessage') }}
                    </div>
                    @endif
                    


                    {{$order->status->name}}

                    <order-alert user_id="{{ auth()->user()->id }}"></order-alert>

                    <hr>
                    <a class="btn btn-primary" href="{{ route('user.orders.create') }}">Vissza a foglalásokhoz</a>
                        @if($order->status_id == 2 && auth()->user()->id == $order->user_id || session('succesMessage') )
                    <a class="btn btn-success" href="{{ route('user.orders.payShow', $order) }}">Tovább a fizetéshez</a>
                        @endif
                </div> <!-- end panel-body -->
            </div> <!-- end panel -->
        </div>
    </div>
</div>
@endsection
