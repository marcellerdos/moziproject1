@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Fizetés</div>

                <div class="panel-body">
                    @if (session('succesMessage'))
                        <div class="alert alert-success">
                            {{ session('succesMessage') }}
                        </div>
                    @elseif(session('denyMessage'))
                    <div class="alert alert-danger">
                        {{ session('denyMessage') }}
                    </div>
                    @endif
                    

                   Rendelés ID: {{$order->id}} <br>
                   Foglalt Szék: {{$order->chair_id}} <br>
                   Dátum: {{$order->created_at}}

                    <order-alert user_id="{{ auth()->user()->id }}"></order-alert>



                    <hr>
                    <a class="btn btn-success" href="{{ route('user.orders.pay', $order) }}">Fizetés</a>
                </div> 
            </div>
        </div>
    </div>
</div>
<script>setTimeout(function(){
    alert('Fizetési idő lejárt! Visszairányítunk a kezdőoldalra.')
    window.location.href="/"
    },120000);</script>
@endsection
