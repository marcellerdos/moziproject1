@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Helyfoglalás</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(session('denyMessage'))
                    <div class="alert alert-danger">
                        {{ session('denyMessage') }}
                    </div>
                    @endif
                    @if(session('succesMessage'))
                    <div class="alert alert-success">
                        {{ session('succesMessage') }}
                    </div>
                    @endif
                    @if(session('payMessage'))
                    <div class="alert alert-success">
                        {{ session('payMessage') }}
                    </div>
                    @endif

                    <order-alert user_id="{{ auth()->user()->id }}"></order-alert>
                    <div class="row">
                        <div class="col-lg-12">
                            <form method="post" action="{{ route('user.orders.store') }}" class="form-horizontal">
                                {{ csrf_field() }}

                                {{-- <div class="form-group"><label class="col-sm-2 control-label">Address</label>
                                    <div class="col-sm-10"><input type="text" name="address" placeholder="Your Address" class="form-control"></div>
                                </div> --}}
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Székek</label>
                                    <div class="col-sm-10">
                                        @if ($chairs == '[]') Nincs szék a moziban. @else
                                        @foreach($chairs as $szek)
                                        <label class="checkbox-inline">
                                        <input type="checkbox" name="toppings[]" value="{{$szek->id}}" id="szek{{$szek->id}}"> Szék {{$szek->id}}
                                        </label>
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>

                                <div class="hr-line-dashed"></div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-success" type="submit">Foglald le!</button>
                                    </div>
                                </div>
                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
    <table class="table table-bordered data-table" id="refreshTable">
        <thead>
            <tr>
                <th>Szék ID</th>
                <th>Szék Státusz</th>
                <th>Státusz</th>
                <th>Törlés</th>
                @csrf
            </tr>
            <form method="post" action="{{ route('user.orders.store') }}" class="form-horizontal">
            @foreach($chairs as $chair)
            <tr>
            <td>{{$chair->id}}</td>
            <td>{{$chair->status_id}}</td>
    <td>@if($chair->status_id == 1) Szabad @elseif($chair->status_id == 2 && $chair->user_id == $user->id) Foglalva általad @elseif($chair->status_id == 2) Foglalt  @elseif($chair->status_id == 3) Elkelt @endif
    </td>
    <td>@if($user->id == $chair->user_id) <a href="{{ route('user.orders.destroy', $chair->id) }}"><button type="button"  class="btn btn-danger btn-sm foglalasTorles" >Törlés</button></a> @endif</td>
            </tr>
                @endforeach
            </form>
        </thead>
        <tbody>
           
        </tbody>
                 </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
